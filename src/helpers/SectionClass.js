// creating a section
import JoinUsSectionComponent from "../components/joinUsSection/joinUsSectionComponent";
import subscribeComponent from "../components/joinUsSection/subscribeComponent";
import unsubscribeComponent from "../components/joinUsSection/unsubscribeComponent";
import bigCommunityComponent from "../components/bigCommunity/bigCommunityComponent";

class SectionClass {
    constructor(titleText, buttonText) {
        this.titleText = titleText;
        this.buttonText = buttonText;
    }

    createJoinUsSection() {
        const htmlElements = JoinUsSectionComponent(this.titleText);
        const [mySection, formElement, inputElement, buttonElement, h3Element] = htmlElements;

        if (!localStorage.getItem("email")) {
            subscribeComponent(
                formElement,
                inputElement,
                buttonElement,
                h3Element,
                this.buttonText
            );
        } else {
            unsubscribeComponent(
                formElement,
                buttonElement
            );
        }

        console.log(localStorage.getItem("email"));
        return mySection;
    }

    createBigCommunitySection() {
        const htmlElements = bigCommunityComponent();
        const [bigCommunitySection] = htmlElements;

        return bigCommunitySection;
    }
}

export default SectionClass;
