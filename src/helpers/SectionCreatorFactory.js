import SectionClass from "./SectionClass";

class SectionCreatorFactory {
    constructor() {
    }

    create(type) {
        switch (type) {
            case "standard":
                return new SectionClass("Join Our Program", "Subscribe")
                    .createJoinUsSection();
            case "advanced":
                return new SectionClass("Join Our Advanced Program", "Subscribe to Advanced Program")
                    .createJoinUsSection();
            case "bigCommunity":
                return new SectionClass()
                    .createBigCommunitySection();
            default:
                console.error("invalid type in sectionCreatorFactory ");
        }
        return null;
    }
}

export default SectionCreatorFactory;
