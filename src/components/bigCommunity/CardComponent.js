export default function CardComponent(avatar, firstName, lastName, position) {
    const fullNameString = `${firstName} ${lastName}`;
    const companyNameString = position.replace("at ", "at <br /> ");

    const card = document.createElement("div");
    card.classList.add("card");

    const img = document.createElement("img");
    img.classList.add("card__img");
    img.src = avatar;

    const description = document.createElement("p");
    description.classList.add("card__description");
    description.innerHTML = "Lorem ipsum dolor sit amet, <br /> "
        + "consectetur adipiscing elit, sed do <br /> "
        + "eiusmod tempor incididunt ut <br /> "
        + "labore et dolor. <br /><br />";

    const fullName = document.createElement("h3");
    fullName.classList.add("card__full-name");
    fullName.innerHTML = fullNameString;

    const companyName = document.createElement("h3");
    companyName.classList.add("card__company-name");
    companyName.innerHTML = companyNameString;

    card.appendChild(img);
    card.appendChild(description);
    card.appendChild(fullName);
    card.appendChild(companyName);

    return card;
}
