import { api } from "../../api";
import CardComponent from "./CardComponent";

export default function bigCommunityComponent() {
    const bigCommunitySection = document.createElement("section");
    bigCommunitySection.classList.add("app-section");

    // filling content of the section

    // h2element
    const h2Element = document.createElement("h2");
    h2Element.innerHTML = "Big Community of <br/> People Like You ";
    h2Element.classList.add("app-title");

    // h3element
    const h3Element = document.createElement("h3");
    h3Element.innerHTML = "We’re proud of our products, and we’re really excited <br /> when we get feedback from our users.";
    h3Element.classList.add("app-subtitle");

    const usersList = document.createElement("div");
    usersList.classList.add("app-section__users-list");

    bigCommunitySection.appendChild(h2Element);
    bigCommunitySection.appendChild(h3Element);
    bigCommunitySection.appendChild(usersList);

    // cards
    setTimeout(async () => {
        const response = await fetch(`${api}/community`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json;charset=utf-8"
            },
        });

        const result = await response.json();

        if (result.error) {
            alert(result.error);
        } else {
            result.forEach((user) => {
                const card = CardComponent(
                    user.avatar,
                    user.firstName,
                    user.lastName,
                    user.position
                );
                usersList.appendChild(card);
            });
        }
    }, 0);

    return [bigCommunitySection, h2Element];
}
