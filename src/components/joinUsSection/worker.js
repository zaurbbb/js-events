let eventsArr = [];

self.onmessage = function (e) {
  console.log("eventsArr", eventsArr);
  const data = e.data;

  if (eventsArr.length === 5) {
    self.postMessage(eventsArr);
    eventsArr = [];
  }

  if (data.type === 'clickEvent' || data.type === 'changeEvent') {
    eventsArr.push(data);
  }
};
