import { api } from "../../api";

export default function unsubscribeComponent(formElement, buttonElement) {
    buttonElement.value = "Unsubscribe";
    buttonElement.addEventListener("click", (e) => {
        e.preventDefault();
        buttonElement.disabled = true;
        setTimeout(async () => {
            const response = await fetch(`${api}/unsubscribe`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json;charset=utf-8"
                },
                // body: JSON.stringify(localStorage.getItem("email"))
            });
            const result = await response.json();
            console.log(result);

            if (result.error) {
                console.log(result);
            } else {
                localStorage.removeItem("email");
                window.location.reload();
            }
        }, 0);
    });

    formElement.appendChild(buttonElement);
}
