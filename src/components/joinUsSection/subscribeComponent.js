import validate from "../../helpers/emailValidator";
import { api } from "../../api";

export default function subscribeComponent(
    formElement,
    inputElement,
    buttonElement,
    h3Element,
    buttonText
) {
    const worker = new Worker(new URL("./worker.js", import.meta.url));
    
    inputElement.classList.add("app-section__input");
    inputElement.type = "text";
    inputElement.placeholder = "Email";

    inputElement.addEventListener("change", (e) => {
        e.preventDefault();
        worker.postMessage({
            type: 'clickEvent',
            desc: 'input value has been changed'
        });
    });

    buttonElement.value = buttonText;
    buttonElement.addEventListener("click", () => {
        const body = { email: inputElement.value };
        worker.postMessage({
            type: 'clickEvent',
            desc: 'subscribe btn has been clicked'
        });

        if (validate(inputElement.value)) {
            buttonElement.disabled = true;
            setTimeout(async () => {
                inputElement.value = "";
                const response = await fetch(`${api}/subscribe`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json;charset=utf-8"
                    },
                    body: JSON.stringify(body)
                });

                const result = await response.json();

                if (result.error) {
                    alert(result.error);
                } else {
                    localStorage.setItem("email", JSON.stringify(body));
                    window.location.reload();
                }
            }, 0);
        } else {
            if (document.querySelector(".app-section__error-message")) {
                document.querySelector(".app-section__error-message").remove();
            }
            const errorMessage = document.createElement("span");
            errorMessage.classList.add("app-section__error-message");
            errorMessage.innerHTML = "Please enter a valid email address <br />";
            h3Element.appendChild(errorMessage);
        }
    });


    worker.onmessage = (e) => {
        let eventsArr = e.data
        console.log("worker.onmessage ", eventsArr);

        fetch('http://localhost:3000/analytics/user', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({eventsArr})
        }).then(response => {
            console.log('req has been sent successfully', response);
        }).catch(error => {
            console.error('an error occurred', error);
        })
    }

    // event listener for the subscribeComponent button
    buttonElement.addEventListener("click", (e) => {
        e.preventDefault();
        console.log(e.target.value);
    });

    formElement.appendChild(inputElement);
    formElement.appendChild(buttonElement);
}
