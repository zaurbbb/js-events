export default function JoinUsSectionComponent(titleText) {
    const joinUsSection = document.createElement("section");
    joinUsSection.classList.add("app-section", "app-section--image-people");

    // filling content of the section

    // h2element
    const h2Element = document.createElement("h2");
    h2Element.innerHTML = titleText;
    h2Element.classList.add("app-title");

    // h3element
    const h3Element = document.createElement("h3");
    h3Element.innerHTML = "Sed do eiusmod tempor incididunt <br />ut labore et dolore magna aliqua.<br />";
    h3Element.classList.add("app-subtitle");

    // form element
    const formElement = document.createElement("form");
    const inputElement = document.createElement("input");
    const buttonElement = document.createElement("input");

    formElement.classList.add("app-section__form");
    buttonElement.type = "submit";

    buttonElement.classList.add("app-section__button", "app-section__button--subscribe");

    joinUsSection.appendChild(h2Element);
    joinUsSection.appendChild(h3Element);
    joinUsSection.appendChild(formElement);

    return [joinUsSection, formElement, inputElement, buttonElement, h3Element];
}
