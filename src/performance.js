export default function measureFetchCommunity () {
  const fetchStart = performance.now();
  console.log("fetchStart " + fetchStart);

  fetch('http://localhost:3000/community')
    .then(() => {
      const fetchEnd = performance.now();
      const fetchTime = fetchEnd - fetchStart;
      const pageLoadSpeed = measurePageLoadSpeed()
      const pageMemoryUsage = measurePageMemoryUsage()

      const performanceMetrics = {
        fetchTime,
        pageLoadSpeed,
        pageMemoryUsage
      };

      console.log(performanceMetrics)

      fetch('http://localhost:3000/analytics/performance', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(performanceMetrics)
      })
        .then(res => console.log(res))
        .catch(error => console.log(error));
    })
    .catch(error => console.log(error));
};

function measurePageLoadSpeed() {
  let time = performance.timing;
  return time.loadEventStart - time.navigationStart;
}

function measurePageMemoryUsage() {
  let usage;
  if ('memory' in window.performance) {
    usage = performance.memory.usedJSHeapSize
  }
  return usage;
}
