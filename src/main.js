import "./styles/style.css";
import SectionCreatorFactory from "./helpers/SectionCreatorFactory";
import measureFetchCommunity from "./performance";

// mySection === joinOurProgramSection
window.addEventListener("DOMContentLoaded", () => {
    const sectionCreator = new SectionCreatorFactory();
    // inserting the mySection
    const appContainer = document.querySelector("#app-container");

    const joinOurProgramSection = sectionCreator.create("standard");
    const bigCommunitySection = sectionCreator.create("bigCommunity");
    appContainer.insertBefore(
        joinOurProgramSection,
        appContainer.children[4]
    );

    appContainer.insertBefore(
        bigCommunitySection,
        appContainer.children[3]
    );

    measureFetchCommunity();
});
